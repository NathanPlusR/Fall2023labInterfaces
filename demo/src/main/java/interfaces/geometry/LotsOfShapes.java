package interfaces.geometry;

public class LotsOfShapes {
    public static void main(String[] args)
    {
        Shape[] shapes = new Shape[5];
        shapes[0] = new Rectangle(2, 4);
        shapes[1] = new Rectangle(3, 6);
        shapes[2] = new Circle(2);
        shapes[3] = new Circle(3);
        shapes[4] = new Square(4);

        for (int i = 0; i < shapes.length; i++)
        {
            System.out.println(shapes[i].getPerimeter());
        }
    }
}
